"""
Idea is to implement a Facade over boto3 ServiceCatalog API that supports basic PsychCore use-cases.
- Will also serve as a "usability" guide and documentation of how to use Service Catalog.
- Used for automation and Web Console replacement.
- At time of writing, cannot operate ServiceCatalog resources with CloudFormation

Author: Mike Gilson
Creation Date: 9-27-17
"""
from argparse import ArgumentParser
import subprocess
import uuid
import boto3
import shlex
from datetime import date, datetime
import json
import os.path
from botocore.client import ClientError
from enum import Enum, auto


class AutoName(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return name


class Modes(AutoName):
    PERMISSIONS = auto()
    PORTFOLIO = auto()
    PRODUCT = auto()


class PermissionActions(AutoName):
    CREATE = auto()
    DELETE = auto()
    ADD_END_USER = auto()


class PortfolioActions(AutoName):
    CREATE = auto()
    LIST = auto()
    DELETE = auto()
    ADD_PRODUCT = auto()


class ProductActions(AutoName):
    CREATE = auto()
    LIST = auto()
    DELETE = auto()
    ADD_CONSTRAINT = auto()
    PROVISION = auto()
    ADD_PROVISION_ARTIFACT = auto()


class Service(AutoName):
    s3 = auto()
    iam = auto()
    servicecatalog = auto()

LAUNCH_ROLE_NAME = 'ServiceCatalogLaunchRole'


def names_for_enum(enum_type):
    return [member.name for member in enum_type ]


def is_mode(mode_enum_member, mode):
    return mode == mode_enum_member.name


def is_action(action_enum_member, action):
    return action == action_enum_member.name


def is_service(service_enum_member, service):
    return service == service_enum_member.name


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))


class BotoClientFactory(object):
    """
    Holds and serves boto3 client singletons
    """

    s3_client = boto3.client(Service.s3.name)
    iam_client = boto3.client(Service.iam.name)
    sc_client = boto3.client(Service.servicecatalog.name)

    @staticmethod
    def client_for(service_namespace):
        if not service_namespace in names_for_enum(Service):
            raise ValueError('Unsupported service_namespace: {}'.format(service_namespace))

        if is_service(Service.s3, service_namespace):
            return BotoClientFactory.s3_client
        elif is_service(Service.iam, service_namespace):
            return BotoClientFactory.iam_client
        elif is_service(Service.servicecatalog, service_namespace):
            return BotoClientFactory.sc_client
        else:
            raise ValueError('Unexpected service_namespace: {}'.format(service_namespace))


class Permissions(object):
    """
    Responsible for creating Service Catalog Administrator and End user IAM permissions.

      - Possibly through invocation of CloudFormation template, Permissions.yaml
    """
    STACK_NAME_PREFIX = 'service-cat-perms'

    CREATE_STACK_CMD_TMPL = ' '.join((
        "aws cloudformation create-stack",
        "--stack-name {STACK_NAME}",
        "--template-body file://{TEMPLATE_PATH}",
        "--capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM"
    ))

    DELETE_STACK_CMD_TMPL = ' '.join((
        "aws cloudformation delete-stack",
        "--stack-name {STACK_NAME}"
    ))

    def __init__(self, stack_name, template_path='Permissions.yaml', client=BotoClientFactory.client_for(Service.iam.name)):
        self.stack_name = stack_name
        self.template_path = template_path
        self.iam_client = client

    def create_permissions_stack(self):
        print('Creating stack..')
        try:
            create_stack_cmd = Permissions.CREATE_STACK_CMD_TMPL.format(STACK_NAME=self.stack_name, TEMPLATE_PATH=self.template_path)
            completed_process = subprocess.run(create_stack_cmd, shell=True, check=True, stdout=subprocess.PIPE)

            print('Success initiating launch.')
            print('stdout: {}'.format(completed_process.stdout))
            print('stderr: {}'.format(completed_process.stderr))
        except subprocess.CalledProcessError as e:
            print('create-stack returned with non-zero exit code.')
            raise e

    def permissions_stack_exists(self):
        pass

    def delete_permissions_stack(self, group_name):

        # First must remove users registered to end-user group, or else stack delete will fail
        print('Removing end users..')
        try:
            self.deregister_end_users(group_name=group_name)
        except Exception as e:
            print('Failed deregistering users...')
            raise e

        print('Deleting stack..')
        try:
            delete_stack_cmd = Permissions.DELETE_STACK_CMD_TMPL.format(STACK_NAME=self.stack_name)
            completed_process = subprocess.run(delete_stack_cmd, shell=True, check=True, stdout=subprocess.PIPE)

            print('Success initiating delete.')
            print('stdout: {}'.format(completed_process.stdout))
            print('stderr: {}'.format(completed_process.stderr))
        except subprocess.CalledProcessError as e:
            print('delete-stack returned with non-zero exit code.')
            raise e

    def deregister_end_users(self, group_name):
        """
        Use boto3 to iterate over group members, degistering them

        :param group_name:
        :return:
        """
        assert group_name.startswith(Permissions.STACK_NAME_PREFIX), 'group_name must start with: {}'.format(Permissions.STACK_NAME_PREFIX)

        response = self.iam_client.get_group(GroupName=group_name)

        # don't support pagination or partial-responses for now
        if response['IsTruncated']:
            raise Exception('get_group resonse truncated. Too many users or other failure to return all.')

        print('Found {num_users} users in group {group}'.format(num_users=len(response['Users']), group=group_name))

        for user in response['Users']:
            self.deregister_end_user(user_name=user['UserName'], group_name=group_name)

    def deregister_end_user(self, user_name, group_name):
        assert group_name.startswith(Permissions.STACK_NAME_PREFIX), 'group_name must start with: {}'.format(Permissions.STACK_NAME_PREFIX)

        print('Attempting to remove {user} from {group}'.format(user=user_name, group=group_name))

        response = self.iam_client.remove_user_from_group(
            GroupName=group_name,
            UserName=user_name
        )

        print('remove_user_from_group response: '.format(response))

    def register_end_user(self, user_name, group_name):
        """
        Attempt to add user, user_name, to EndUserGroup, group_name, created in stack

        :param user_name:
        :param group_name:
        :return:
        """
        assert group_name.startswith(Permissions.STACK_NAME_PREFIX), 'group_name must start with: {}'.format(Permissions.STACK_NAME_PREFIX)

        print('Attempting to add {user} to {group}'.format(user=user_name, group=group_name))

        response = self.iam_client.add_user_to_group(
            GroupName=group_name,
            UserName=user_name
        )

        print('add_user_to_group response: '.format(response))


class Portfolio(object):

    def __init__(self, sc_client=BotoClientFactory.client_for(Service.servicecatalog.name)):
        self.sc_client = sc_client

    def create(self, display_name, description, provider):
        print('Attempting to create portfolio..')

        response = self.sc_client.create_portfolio(
            DisplayName=display_name,
            Description=description,
            ProviderName=provider,
            Tags=[
                {
                    'Key': 'Name',
                    'Value': 'service-catalog-resource'
                }
            ]
        )

        print('create_portfolio response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def list(self):
        print('Attempting to list portfolios..')

        response = self.sc_client.list_portfolios()

        print('list_portfolios response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def delete(self, portfolio_id):
        print('Attempting to delete portfolio: {}'.format(portfolio_id))
        response = self.sc_client.delete_portfolio(Id=portfolio_id)
        print('list_portfolios response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def add_product(self, portfolio_id, product_id):
        print('Attempting to add {} to {}'.format(product_id, portfolio_id))

        response = self.sc_client.associate_product_with_portfolio(
            ProductId=product_id,
            PortfolioId=portfolio_id
        )

        print('associate_product_with_portfolio response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))


class Constraint(object):

    LAUNCH_TYPE = 'LAUNCH'
    NOTIFICATION_TYPE = 'NOTIFICATION'
    TEMPLATE_TYPE = 'TEMPLATE'

    LAUNCH_CONSTRAINT_PARAM_PROP = 'RoleArn'
    NOTIFICATION_CONSTRAINT_PARAM_PROP = 'NotificationArns'
    TEMPLATE_CONSTRAINT_PARAM_PROP = 'Rules'

    def __init__(self, portfolio_id, product_id, sc_client=BotoClientFactory.client_for(Service.servicecatalog.name),
                 iam_client=BotoClientFactory.client_for(Service.iam.name)):

        self.sc_client = sc_client
        self.iam_client = iam_client
        self.portfolio_id = portfolio_id
        self.product_id = product_id

    def create_launch_constraint(self):

        print('Creating launch constraint...')

        launch_role_arn = self.arn_for_launch_role()
        if not launch_role_arn:
            raise ValueError('Failed to find Launch Role. Have Permissions been created?')
        else:
            print("Found launch role, with arn: {}".format(launch_role_arn))

        response = self.sc_client.create_constraint(
            PortfolioId=self.portfolio_id,
            ProductId=self.product_id,
            Parameters=json.dumps({
                Constraint.LAUNCH_CONSTRAINT_PARAM_PROP: launch_role_arn
            }),
            Type=Constraint.LAUNCH_TYPE,
            IdempotencyToken=str(uuid.uuid1())
        )

        print('create_constraint response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def arn_for_launch_role(self):

        try:
            response = self.iam_client.get_role(
                RoleName=LAUNCH_ROLE_NAME
            )
            return response['Role']['Arn']
        except ClientError as ce:
            return None


class Template(object):

    def __init__(self, local_path, bucket_name, key_name, s3_client=BotoClientFactory.client_for(Service.s3.name)):

        # check if local template file exists
        if not os.path.isfile(local_path):
            raise ValueError('Could not find template file, {}, to upload to S3'.format(local_path))

        # check if bucket exists...
        try:
            s3_client.head_bucket(Bucket=bucket_name)
        except ClientError as ce:
            print('Could not find bucket, {}, in S3.'.format(bucket_name))
            print('Or bucket exists, but did not have HEAD_OBJECT permissions on it.')
            raise ce

        self.s3_client = s3_client
        self.local_path = local_path
        self.bucket_name = bucket_name
        self.key_name = key_name

    def upload(self):
        print('Attempting to upload {templ} to s3://{bucket}/{key}..'.format(templ=self.local_path, bucket=self.bucket_name, key=self.key_name))

        response = self.s3_client.put_object(
            Body=open(self.local_path, 'rb'),
            Bucket=self.bucket_name,
            Key=self.key_name
        )

        print('search_products response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def object_exists(self):
        print('Attempting to test existence of s3://{bucket}/{key}..'.format(bucket=self.bucket_name, key=self.key_name))
        try:
            response = self.s3_client.head_object(
                Bucket=self.bucket_name,
                Key=self.key_name
            )
            print('head_object response: ')
            print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))
            return ('ContentLength' in response) and (response['ContentLength'] > 0)
        except ClientError as ce:
            print('Could not find object, {}, in S3.'.format(self.https_url()))
            print('Or object exists, but did not have HEAD_OBJECT permissions on it.')
            print(ce)
            return False

    def https_url(self):
        return '/'.join((
            'https://s3.amazonaws.com',
            self.bucket_name,
            self.key_name
        ))


class Product(object):

    PRODUCT_STATE_INIT = 0
    PRODUCT_STATE_CREATED = 1

    def __init__(self, sc_client=BotoClientFactory.client_for(Service.servicecatalog.name)):
        self.sc_client = sc_client

    def create(self, template, name, owner, description):
        print('Attempting to create Product..')

        template.upload()

        if not template.object_exists():
            raise ValueError('Cannot create product: Unable to upload/find template on S3')

        #
        # Service Catalog will validate the template
        #
        response = self.sc_client.create_product(
            Name=name,
            Owner=owner,
            Description=description,
            ProductType='CLOUD_FORMATION_TEMPLATE',
            Tags=[
                {
                    'Key': 'Name',
                    'Value': 'ClinEx-CreateProductTag'
                },
            ],
            ProvisioningArtifactParameters={
                # 'Name': 'string',
                # 'Description': 'string',
                'Info': {
                    'LoadTemplateFromURL': template.https_url(),
                },
                'Type': 'CLOUD_FORMATION_TEMPLATE'
            }
        )

        print('create_product response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def create_provisioning_artifact(self, product_id, template):
        print('Attempting to create provisioning artifact..')

        response = self.sc_client.create_provisioning_artifact(
            ProductId=product_id,
            Parameters={
                'Description': 'CreateProvArtifact',
                'Info': {
                    'LoadTemplateFromURL': template.https_url()
                },
                'Type': 'CLOUD_FORMATION_TEMPLATE'
            }
        )

        print('create_provisioning_artifact response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def list(self):
        print('Attempting to list products..')

        response = self.sc_client.search_products_as_admin()

        print('search_products response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def provision(self, product_id, provisioning_artifact_id, provisioned_name):
        print('Attempting to provision products..')

        response = self.sc_client.provision_product(
            ProductId=product_id,
            # The provisioning artifact identifier for this product. This is sometimes referred to as the product version.
            ProvisioningArtifactId=provisioning_artifact_id,
            # PathId='string',
            ProvisionedProductName=provisioned_name,
            # ProvisioningParameters=[
            #     {
            #         'Key': 'string',
            #         'Value': 'string'
            #     },
            # ],
            # Tags=[
            #     {
            #         'Key': 'string',
            #         'Value': 'string'
            #     },
            # ],
            # NotificationArns=[
            #     'string',
            # ],
            # ProvisionToken='string'
        )

        print('provision_product response: ')
        print(json.dumps(response, indent=4, sort_keys=False, default=json_serial))

    def delete(self):
        pass

    def describe(self):
        pass

    def update(self):
        pass


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('--mode', type=str, required=True)
    parser.add_argument('--action', type=str, required=True)
    parser.add_argument('--action-arg', type=str, required=False)
    args = parser.parse_args()

    assert args.mode in names_for_enum(Modes), 'Mode argument must be one of {}'.format(names_for_enum(Modes))

    return args.mode, args.action, args.action_arg


def main(mode, action, action_arg):

    if is_mode(Modes.PERMISSIONS, mode):

        if is_action(PermissionActions.CREATE, action):

            stack_name = '-'.join((Permissions.STACK_NAME_PREFIX, str(uuid.uuid4())[0:8]))
            perms = Permissions(stack_name=stack_name, template_path='Permissions.yaml')
            perms.create_permissions_stack()

        elif is_action(PermissionActions.DELETE, action):

            assert action_arg, 'Must supply the Stack Name using --action-arg "stack-name=<stack_name> group-name=<group_name>"'

            stack_and_group = dict(token.split('=') for token in shlex.split(action_arg))

            assert 'stack-name' in stack_and_group, 'Couldnt find stack-name. Must supply the stack and group using --action-arg "stack-name=<user_name> group-name=<group_name>"'
            assert 'group-name' in stack_and_group, 'Couldnt find group-name. Must supply the stack and group using --action-arg "stack-name=<user_name> group-name=<group_name>"'
            assert stack_and_group['stack-name'].startswith(Permissions.STACK_NAME_PREFIX), 'stack_name must start with: {}'.format(Permissions.STACK_NAME_PREFIX)
            assert stack_and_group['group-name'].startswith(Permissions.STACK_NAME_PREFIX), 'group_name must start with: {}'.format(Permissions.STACK_NAME_PREFIX)

            perms = Permissions(stack_name=stack_and_group['stack-name'])
            perms.delete_permissions_stack(stack_and_group['group-name'])

        elif is_action(PermissionActions.ADD_END_USER, action):

            assert action_arg, 'Must supply the user and group using --action-arg "stack-name=<stack_name> user-name=<user_name> group-name=<group_name>"'

            stack_user_and_group = dict(token.split('=') for token in shlex.split(action_arg))

            print(stack_user_and_group)

            assert 'stack-name' in stack_user_and_group, 'Couldnt find stack-name.'
            assert 'user-name' in stack_user_and_group, 'Couldnt find user-name.'
            assert 'group-name' in stack_user_and_group, 'Couldnt find group-name.'

            perms = Permissions(stack_name=stack_user_and_group['stack-name'])
            perms.register_end_user(user_name=stack_user_and_group['user-name'], group_name=stack_user_and_group['group-name'])
        else:
            raise Exception('Did not understand action for PERMS mode: {}'.format(action))

    elif is_mode(Modes.PORTFOLIO, mode):

        if is_action(PortfolioActions.CREATE, action):

            portfolio = Portfolio()
            portfolio.create(
                display_name='PsychCore Clinical Exome',
                description='Tools and Services for Executing the Clinical Exome Pipeline',
                provider='Random Guy From Ohio'
            )

        elif is_action(PortfolioActions.LIST, action):

            portfolio = Portfolio()
            portfolio.list()

        elif is_action(PortfolioActions.DELETE, action):

            assert action_arg, 'Must supply the portfolio-id using --action-arg <portfolio_id>'

            print(action_arg)

            portfolio = Portfolio()
            portfolio.delete(portfolio_id=action_arg)

        elif is_action(PortfolioActions.ADD_PRODUCT, action):

            assert action_arg, 'Must supply the portfolio-id and product-id using --action-arg "portfolio-id=<portfolio-id > product-id=<product-id >"'

            port_and_prod = dict(token.split('=') for token in shlex.split(action_arg))
            print(port_and_prod )

            assert 'portfolio-id' in port_and_prod, 'Couldnt find portfolio-id.'
            assert 'product-id' in port_and_prod, 'Couldnt find product-id.'

            portfolio = Portfolio()
            portfolio.add_product(
                portfolio_id=port_and_prod['portfolio-id'],
                product_id=port_and_prod['product-id']
            )

        else:
            raise Exception('Did not understand action for PORTFOLIO mode: {}'.format(action))

    elif is_mode(Modes.PRODUCT, mode):

        if is_action(ProductActions.CREATE, action):

            assert action_arg, 'Must supply the name and description using --action-arg "product-name=<name> product-description=<desc>"'

            name_and_desc = dict(token.split('=') for token in shlex.split(action_arg))

            print(name_and_desc)

            assert 'product-name' in name_and_desc, 'Couldnt find product-name. Must supply the user and group using --action-arg "product-name=<name> product-description=<desc>"'
            assert 'product-description' in name_and_desc, 'Couldnt find product-description. Must supply the user and group using --action-arg "product-name=<name> product-description=<desc>"'

            template = Template(
                local_path='ProductTemplate.yaml',
                bucket_name='mike_scratch',
                key_name='ProductTemplate.yaml'
            )

            product = Product()
            product.create(
                template=template,
                name=name_and_desc['product-name'],
                owner='PsychCore',
                description=name_and_desc['product-description'],
            )

        elif is_action(ProductActions.LIST, action):

            product = Product()
            product.list()

        elif is_action(ProductActions.DELETE, action):

            raise Exception('Not implemented')

        elif is_action(ProductActions.ADD_CONSTRAINT, action):

            assert action_arg, 'Must supply the portfolio-id and product-id using --action-arg "portfolio-id=<portfolio-id> product-id=<product-id>"'

            port_and_prod = dict(token.split('=') for token in shlex.split(action_arg))
            print(port_and_prod )

            assert 'portfolio-id' in port_and_prod, 'Couldnt find portfolio-id.'
            assert 'product-id' in port_and_prod, 'Couldnt find product-id.'

            constraint = Constraint(
                portfolio_id=port_and_prod['portfolio-id'],
                product_id=port_and_prod['product-id']
            )

            constraint.create_launch_constraint()

        elif is_action(ProductActions.PROVISION, action):
            # product_id, provisioning_artifact_id, provisioned_name):
            assert action_arg, 'Must supply the portfolio-id and product-id using --action-arg "product-id=<product-id> provisioning_artifact_id=<provisioning_artifact_id>"'

            prod_and_pai = dict(token.split('=') for token in shlex.split(action_arg))
            print(prod_and_pai)

            assert 'product-id' in prod_and_pai, 'Couldnt find product-id.'
            assert 'provisioning_artifact_id' in prod_and_pai, 'Couldnt find provisioning_artifact_id.'

            product = Product()
            product.provision(
                product_id=prod_and_pai['product-id'],
                provisioning_artifact_id=prod_and_pai['provisioning_artifact_id'],
                provisioned_name='depressing-as-hell'
            )

        elif is_action(ProductActions.ADD_PROVISION_ARTIFACT, action):

            assert action_arg, 'Requires action-arg of ProductId'

            template = Template(
                local_path='ProductTemplate.yaml',
                bucket_name='mike_scratch',
                key_name='ProductTemplate.yaml'
            )

            product = Product()
            product.create_provisioning_artifact(
                product_id=action_arg,
                template=template
            )
        else:
            raise Exception('Did not understand action for PRODUCT mode: {}'.format(action))

    else:
        raise Exception('Strange MODE: {}'.format(mode))


if __name__ == '__main__':
    main(
        *parse_args()
    )
