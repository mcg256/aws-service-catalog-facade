#!/usr/bin/env bash

SC_MODULE='service-cat-facade.py'

if [ ! -f ${SC_MODULE} ];
then
    echo "Couldn't find python module: ${SC_MODULE}"
    echo "Exiting"
    exit 1
fi

function create_permissions() {
    python ${SC_MODULE} --mode PERMISSIONS --action CREATE
}

function add_end_user() {
    USER_NAME=${1?Must pass username as arg 1}
    GROUP_NAME=${2?Must pass group as arg 2}

    python ${SC_MODULE} --mode PERMISSIONS --action ADD_END_USER --action-arg "user-name=${USER_NAME} group-name=${GROUP_NAME}"
}

function delete_permissions() {
    STACK_NAME=${1?Must pass stackname as arg 1}
    GROUP_NAME=${2?Must pass groupname as arg 2}

    python ${SC_MODULE} --mode PERMISSIONS --action DELETE --action-arg "stack-name=${STACK_NAME} group-name=${GROUP_NAME}"
}

function create_portfolio() {
    python ${SC_MODULE} --mode PORTFOLIO --action CREATE
}

function list_portfolios() {
    python ${SC_MODULE} --mode PORTFOLIO --action LIST
}

function delete_portfolio() {
    PORTFOLIO_ID=${1?Must pass PORTFOLIO_ID as first arg to function}

    python ${SC_MODULE} --mode PORTFOLIO --action DELETE --action-arg ${PORTFOLIO_ID}
}

function create_product() {
    python ${SC_MODULE} --mode PRODUCT --action CREATE --action-arg "product-name=SomeProduct product-description=\"This is a desc with spaces\""
}

function add_product_to_portfolio() {
    PORT_ID=${1?Must pass portid as arg 1}
    PROD_ID=${2?Must pass prodid as arg 2}

    python ${SC_MODULE} --mode PORTFOLIO --action ADD_PRODUCT --action-arg "portfolio-id=${PORT_ID} product-id=${PROD_ID}"
}

function add_product_constraint() {
    PORT_ID=${1?Must pass portid as arg 1}
    PROD_ID=${2?Must pass prodid as arg 2}

    python ${SC_MODULE} --mode PRODUCT --action ADD_CONSTRAINT --action-arg "portfolio-id=${PORT_ID} product-id=${PROD_ID}"
}

#create_permissions
#add_end_user "MIchaelGilson" service-cat-perms-dd21fe61-EndUserGroup-DNOBIH8XCN1R
#delete_permissions service-cat-perms-dd21fe61 service-cat-perms-dd21fe61-EndUserGroup-DNOBIH8XCN1R
#create_portfolio
#list_portfolios
#delete_portfolio port-k4sadbkpxxw3w
#create_product
#add_product_to_portfolio port-vedk55z7xtfb6 prod-blacx5oyqlozo
add_product_constraint port-vedk55z7xtfb6 prod-blacx5oyqlozo
